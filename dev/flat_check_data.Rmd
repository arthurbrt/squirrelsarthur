---
title: "flat_check_data.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(dplyr)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# check_primary_color_is_ok
    
```{r function-check_primary_color_is_ok}
#' Check the values of primary color
#'
#' @param string Character. A vector with primary colors
#'
#' @return Boolean. True if primary color
#' @export
#'
#' @examples
check_primary_color_is_ok <- function(string) {
  all_colors_OK <- all(
    string %in% c("Gray", "Cinnamon", "Black", NA)
  )
  
  if (isFALSE(all_colors_OK)) {
    stop("All colors are not ok with primary colors: Gray, Black, Cinnamon.")
  }
  
  return(all_colors_OK)
}
```
  
```{r example-check_primary_color_is_ok}
check_primary_color_is_ok(string = "Black")
check_primary_color_is_ok(string = c("Black", "Gray"))
# check_primary_color_is_ok(string = c("Black", "Gray", "Red"))
```
  
```{r tests-check_primary_color_is_ok}
test_that("check_primary_color_is_ok works", {
  expect_true(inherits(check_primary_color_is_ok, "function"))
  
  expect_true(check_primary_color_is_ok(string = "Black"))
  expect_true(check_primary_color_is_ok(string = c("Black", "Gray")))
  
  expect_error(
    check_primary_color_is_ok(string = c("Black", "Gray", "Red")),
    "All colors are not ok with primary colors: Gray, Black, Cinnamon."
  )
})
```

# check_squirrel_data_integrity
    
```{r function-check_squirrel_data_integrity}
#' check_squirrel_data_integrity
#'
#' @param df_squirrels DataFrame.A dataset with squirrels data
#'
#' @return Side effects. A message if everything is ok
#' @export
#'
#' @examples
check_squirrel_data_integrity <- function(df_squirrels) {
  
  is_present_primary_fur_color <- "primary_fur_color" %in% names(df_squirrels)
  
  if (isFALSE(is_present_primary_fur_color)) {
    stop("There is no primary_fur_color in the dataset")
  }
  
  primary_colors_ok <- check_primary_color_is_ok(string = df_squirrels$primary_fur_color)
  
  if (isTRUE(primary_colors_ok)) {
    message("All primary colors are good!")
  }
}
```
  
```{r example-check_squirrel_data_integrity}
nyc_squirrels <- readr::read_csv(
  file = system.file("nyc_squirrels_sample.csv", package = "squirrelsarthur")
)

check_squirrel_data_integrity(df_squirrels = nyc_squirrels)
```
  
```{r tests-check_squirrel_data_integrity}
library(dplyr)
test_that("check_squirrel_data_integrity works", {
  expect_true(inherits(check_squirrel_data_integrity, "function"))
  
  nyc_squirrels <- readr::read_csv(
    file = system.file("nyc_squirrels_sample.csv", package = "squirrelsarthur")
  )

  expect_message(
    check_squirrel_data_integrity(df_squirrels = nyc_squirrels),
    "All primary colors are good!"
  )
  
  nyc_squirrels_wrong <- nyc_squirrels %>% 
    select(-primary_fur_color)

  expect_error(
    check_squirrel_data_integrity(df_squirrels = nyc_squirrels_wrong),
    "There is no primary_fur_color in the dataset"
  )
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(
  flat_file = "dev/flat_check_data.Rmd",
  vignette_name = "Check data", overwrite = TRUE
)
```

